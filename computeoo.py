#!/usr/bin/python3
# -*- coding: utf-8 -*-

import math
import sys


class Compute:

    def __init__(self, default=2):
        """Esto es el método iniciliazador"""
        self.default = default

    def selfdef(self):
        return self.default

    def power(num, self):
        """ Function to compute number to the exp power"""
        return num ** self

    def log(num, self):
        """ Function to compute log base of number"""
        return math.log(num, self)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        # Creo un objeto de la clase Compute y le doy el valor de 2
        num2 = Compute().default

    if sys.argv[1] == "power":
        result = Compute.power(num, num2)
    elif sys.argv[1] == "log":
        result = Compute.log(num, num2)
    else:
        sys.exit('Operand should be power or log')

    print(result)
