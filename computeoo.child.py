

import computeoo
import sys
r1 = computeoo.Compute


class ComputeChild:
    def __init__(self, num2):
        self.num2 = num2

    def set_def(self):
        if num2 <= 0:
            raise ValueError("El número no puede ser menor o igual que 0")
        elif num2 == None:
            return computeoo.Compute().default
        else:
            return float(sys.argv[3])

    def get_def(self):
        return ComputeChild.set_def(num2)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")
    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num3 = computeoo.Compute().default
    else:
        num2 = float(sys.argv[3])
        num3 = ComputeChild.get_def(num2)


    if sys.argv[1] == "power":
        result = r1.power(num, num3)
    elif sys.argv[1] == "log":
        result = r1.log(num, num3)
    else:
        sys.exit('Operand should be power or log')

    print(result)
